<?php

use EntegreX\DataMapper\Download;
use EntegreX\DataMapper\Generate;
use EntegreX\DataMapper\Verbose;
use EntegreX\DataMapper\Output;
use EntegreX\DataMapper\Parse;
use EntegreX\DataMapper\Profiler;
use EntegreX\DataMapper\RuntimeException;
use EntegreX\DataMapper\OutputGroup;

$logData = [];

$handleStatus = false;
$showVerbose = true;

// Download
if ($params['download']) {
    try {
        $downloadInstance = new Download($params['url']);
        $response = $downloadInstance->run();
        unset($downloadInstance);

        if (!$response['success']) {
            throw new RuntimeException($response['error_message'], $response['error_code']);
        }

        // DEBUG
        if ($params['debug']) {
            $logData['download'] = [
                'timestamp' => date('Y-m-d H:i:s'),
                'file_size' => $response['file_size'],
                'profiler'  => Profiler::prepare($response)
            ];

            if (APP_CLI) {
                echo '[DEBUG] Download: ' . $response['file_size'] . 'MB (Memory: ' . $logData['download']['profiler']['memory'] . 'MB | ' . $logData['download']['profiler']['time'] . 's)' . PHP_EOL;
            }
        }

        $handleStatus = true;
    } catch (Throwable $e) {
        throw new RuntimeException('Download RuntimeException: ' . $e->getMessage(), $e->getCode());
    }
}

// Parse
if ($params['parse']) {
    try {
        $parseInstance = new Parse($params['url'], $params['format']);
        $response = $parseInstance->run();
        unset($parseInstance);

        if (!$response['success']) {
            throw new RuntimeException($response['error_message'], $response['error_code']);
        }

        // DEBUG
        if ($params['debug']) {
            $logData['parse'] = [
                'timestamp' => date('Y-m-d H:i:s'),
                'file_size' => $response['file_size'],
                'profiler'  => Profiler::prepare($response)
            ];

            if (APP_CLI) {
                echo '[DEBUG] Parse: ' . $response['file_size'] . 'MB (Memory: ' . $logData['parse']['profiler']['memory'] . 'MB | ' . $logData['parse']['profiler']['time'] . 's)' . PHP_EOL;
            }
        }

        $handleStatus = true;
    } catch (Throwable $e) {
        throw new RuntimeException('Parse RuntimeException: ' . $e->getMessage(), $e->getCode());
    }
}

// Generate
if ($params['generate']) {
    try {
        $generateInstance = new Generate($params['url'], $params['scheme']);
        $response = $generateInstance->run();
        unset($generateInstance);

        if (!$response['success']) {
            throw new RuntimeException($response['error_message'], $response['error_code']);
        }

        // DEBUG
        if ($params['debug']) {
            $logData['generate'] = [
                'timestamp' => date('Y-m-d H:i:s'),
                'file_size' => $response['file_size'],
                'profiler'  => Profiler::prepare($response)
            ];

            if (APP_CLI) {
                echo '[DEBUG] Generate: ' . $response['file_size'] . 'MB (Memory: ' . $logData['generate']['profiler']['memory'] . 'MB | ' . $logData['generate']['profiler']['time'] . 's)' . PHP_EOL;
            }
        }

        $handleStatus = true;
    } catch (Throwable $e) {
        throw new RuntimeException('Generate RuntimeException: ' . $e->getMessage(), $e->getCode());
    }
}

// Output
if ($params['output']) {
    try {
        $outputInstance = new Output($params['url']);
        $response = $outputInstance->run();
        unset($outputInstance);

        if (!$response['success']) {
            throw new RuntimeException($response['error_message'], $response['error_code']);
        }

        // DEBUG
        if ($params['debug']) {
            $logData['output'] = [
                'timestamp' => date('Y-m-d H:i:s'),
                'file_size' => $response['file_size'],
                'profiler'  => Profiler::prepare($response)
            ];

            if (APP_CLI) {
                echo '[DEBUG] Output: ' . $response['file_size'] . 'MB (Memory: ' . $logData['output']['profiler']['memory'] . 'MB | ' . $logData['output']['profiler']['time'] . 's)' . PHP_EOL;
            }
        }

        if (!APP_CLI) {
            $showVerbose = false;
            
            $output = json_decode($response['data'], true);

            // Group
            if (!empty($_GET['group'])) {
                $output = OutputGroup::run($output, $_GET['group']);
            }

            echo json_encode($output);
        }

        $handleStatus = true;
    } catch (Throwable $e) {
        throw new RuntimeException('Output RuntimeException: ' . $e->getMessage(), $e->getCode());
    }
}

// Verbose
if ($params['verbose']) {
    try {
        $verboseInstance = new Verbose($params['url']);
        $response = $verboseInstance->run();
        unset($verboseInstance);

        if (!$response['success']) {
            throw new RuntimeException($response['error_message'], $response['error_code']);
        }

        echo $response['data'];

        $handleStatus = true;
        $showVerbose = false;
    } catch (Throwable $e) {
        throw new RuntimeException('Verbose RuntimeException: ' . $e->getMessage(), $e->getCode());
    }
}

if (!$handleStatus) {
    throw new RuntimeException('Are you kola?');
}