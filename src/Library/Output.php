<?php

namespace EntegreX\DataMapper;

use Exception;

class Output
{
    private $_url;

    /**
     * Parse constructor.
     *
     * @param string $url
     *
     * @throws Exception
     */
    public function __construct(string $url)
    {
        URL::validate($url);

        $this->_url = $url;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $profiler = [
            'time'   => microtime(true),
            'memory' => memory_get_usage(true)
        ];

        $dataPath = File::getDataPath($this->_url);

        if (!file_exists($dataPath)) {
            throw new RuntimeException('Data file not found!');
        }

        $fileSize = filesize($dataPath);
        if (!$fileSize) {
            throw new RuntimeException('Data file is empty!');
        }

        $data = file_get_contents($dataPath);
        if (!$data) {
            throw new RuntimeException('Data is empty!');
        }

        return [
            'success'   => true,
            'profiler'  => [
                'memory' => $profiler['memory'],
                'time'   => $profiler['time']
            ],
            'file_size' => Profiler::byte2mb($fileSize,2),
            'data'      => $data
        ];
    }
}