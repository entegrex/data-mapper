<?php

namespace EntegreX\DataMapper;

use Exception;

class Verbose
{
    private $_url;

    /**
     * Log constructor.
     *
     * @param string $url
     *
     * @throws Exception
     */
    public function __construct(string $url)
    {
        URL::validate($url);

        $this->_url = $url;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function run()
    {
        $logDataPath = File::getVerbosePath($this->_url);

        if (!file_exists($logDataPath)) {
            throw new RuntimeException('Log file not found!');
        }

        if (!filesize($logDataPath)) {
            throw new RuntimeException('Log file is empty!');
        }

        return ['success' => true, 'data' => file_get_contents($logDataPath)];
    }

    public static function save(string $url, array $data)
    {
        if (!$data) {
            return false;
        }

        $logPath = File::getVerbosePath($url);
        if (file_exists($logPath)) {
            $logContent = json_decode(file_get_contents($logPath), true);
            $logContent = array_merge($logContent, $data);
        } else {
            $logContent = $data;
        }

        file_put_contents($logPath, json_encode($logContent));

        return true;
    }
}