<?php

namespace EntegreX\DataMapper;

class File
{
    const TMP_PATH = BASE_PATH . '/app/Storage/tmp/';

    public static function generateName(string $url): string
    {
        return md5($url);
    }

    public static function getRawPath(string $url): string
    {
        return self::TMP_PATH . self::generateName($url) . '_raw';
    }

    public static function getRawDataPath(string $url): string
    {
        return self::TMP_PATH . self::generateName($url) . '_raw.json';
    }

    public static function getDataPath(string $url): string
    {
        return self::TMP_PATH . self::generateName($url) . '.json';
    }

    public static function getVerbosePath(string $url): string
    {
        return self::TMP_PATH . self::generateName($url) . '_verbose.json';
    }
}