<?php

namespace EntegreX\DataMapper;

use Exception;

class URL
{
    /**
     * @param string $url
     *
     * @throws Exception
     */
    public static function validate(string $url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new RuntimeException('URL not validated!');
        }
    }
}