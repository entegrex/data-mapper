<?php

namespace EntegreX\DataMapper;

class Profiler
{
    public static function prepare(array $data): array
    {
        $output['time'] = round(microtime(true) - $data['profiler']['time'], 4);
        $output['memory'] = self::byte2mb(memory_get_usage(true) - $data['profiler']['memory']);

        return $output;
    }

    public static function byte2mb(int $byte, $precision = 4): float
    {
        return round($byte / 1048576, $precision);
    }
}