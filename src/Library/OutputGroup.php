<?php

namespace EntegreX\DataMapper;

class OutputGroup
{
    public static function run(array $items, string $fieldCode): array
    {
        if (!$fieldCode) {
            return $items;
        }

        $output = [];
        foreach ($items as $item) {
            if (empty($item[$fieldCode])) {
                continue;
            }

            if (!isset($output[$item[$fieldCode]])) {
                $output[$item[$fieldCode]] = 1;
            } else {
                $output[$item[$fieldCode]]++;
            }
        }

        return $output;
    }
}