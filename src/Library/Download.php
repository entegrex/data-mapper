<?php

namespace EntegreX\DataMapper;

use Exception;

class Download
{
    private $_url;

    /**
     * Download constructor.
     *
     * @param string $url
     *
     * @throws Exception
     */
    public function __construct(string $url)
    {
        URL::validate($url);

        $this->_url = $url;
    }

    public function run(): array
    {
        $profiler = [
            'time'   => microtime(true),
            'memory' => memory_get_usage(true)
        ];

        $rawPath = File::getRawPath($this->_url);

        $url = str_replace('http://localhost/', BASE_PATH . '/public/', $this->_url);
        file_put_contents($rawPath, file_get_contents($url));

        return [
            'success'   => true,
            'profiler'  => [
                'memory' => $profiler['memory'],
                'time'   => $profiler['time']
            ],
            'file_size' => Profiler::byte2mb(filesize($rawPath), 2)
        ];
    }
}