<?php

namespace EntegreX\DataMapper;

use Exception;
use Sabre\Xml\Service;

class Parse
{
    private $_url;
    private $_format;

    /**
     * Parse constructor.
     *
     * @param string $url
     * @param string $format
     *
     * @throws Exception
     */
    public function __construct(string $url, string $format)
    {
        URL::validate($url);

        if ($format != 'xml') {
            throw new RuntimeException('Format is not allowed!');
        }

        $this->_url = $url;
        $this->_format = $format;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $profiler = [
            'time'   => microtime(true),
            'memory' => memory_get_usage(true)
        ];

        $rawFilePath = File::getRawPath($this->_url);

        if (!file_exists($rawFilePath)) {
            throw new RuntimeException('RAW file not found!');
        }

        if (!filesize($rawFilePath)) {
            throw new RuntimeException('RAW file is empty!');
        }

        $data = null;
        if ($this->_format == 'xml') {
            $service = new Service();
            $response = $service->parse(file_get_contents($rawFilePath), null, $rootElementName);
            $data = json_encode(['root_element_name' => $rootElementName, 'payload' => $response]);
        }

        if (!$data) {
            throw new RuntimeException('Data is empty!');
        }

        $rawDataPath = File::getRawDataPath($this->_url);
        file_put_contents($rawDataPath, $data);

        return [
            'success'   => true,
            'profiler'  => [
                'memory' => $profiler['memory'],
                'time'   => $profiler['time']
            ],
            'file_size' => Profiler::byte2mb(filesize($rawDataPath), 2),
            'data'      => $data
        ];
    }

    public static function convert($value)
    {
        if (!$value) {
            return null;
        }

        if (!is_array($value)) {
            return $value;
        }

        $output = [];
        foreach ($value as $valueItem) {
            $_valueItem = $valueItem;
            unset($_valueItem['name']);
            $_valueItem['value'] = self::convert($_valueItem['value']);

            $output[self::_nameFix($valueItem['name'])][] = $_valueItem;
        }

        return $output;
    }

    public static function convert2KeyValue(array $data): array
    {
        $output = [];
        foreach ($data['value'] as $item) {
            $output[self::_nameFix($item['name'])] = $item['value'];
        }

        return $output;
    }

    private static function _nameFix(string $name)
    {
        return str_replace('{}', '', $name);
    }
}