<?php

namespace EntegreX\DataMapper;

use Exception;

class Generate
{
    private $_url;
    private $_scheme;

    /**
     * Parse constructor.
     *
     * @param string $url
     * @param string $scheme
     *
     * @throws Exception
     */
    public function __construct(string $url, string $scheme)
    {
        URL::validate($url);

        if (!$scheme) {
            throw new RuntimeException('Scheme not defined!');
        }

        $scheme = 'P5TE6X\\Scheme\\' . ucfirst($scheme);
        if (!class_exists($scheme)) {
            throw new RuntimeException('Scheme not found!');
        }

        $this->_url = $url;
        $this->_scheme = $scheme;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $profiler = [
            'time'   => microtime(true),
            'memory' => memory_get_usage(true)
        ];

        $RawDataFilePath = File::getRawDataPath($this->_url);

        if (!file_exists($RawDataFilePath)) {
            throw new RuntimeException('Data file not found!');
        }

        if (!filesize($RawDataFilePath)) {
            throw new RuntimeException('Data file is empty!');
        }

        $data = json_decode(file_get_contents($RawDataFilePath), true);
        if (json_last_error()) {
            throw new RuntimeException('[JSON] ' . json_last_error_msg(), json_last_error());
        }

        if (!$data) {
            throw new RuntimeException('Data is empty!');
        }

        $dataPath = File::getDataPath($this->_url);
        file_put_contents($dataPath, call_user_func([$this->_scheme, 'run'], $data));

        return [
            'success'   => true,
            'profiler'  => [
                'memory' => $profiler['memory'],
                'time'   => $profiler['time']
            ],
            'file_size' => Profiler::byte2mb(filesize($dataPath),2)
        ];
    }
}