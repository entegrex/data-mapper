<?php

function p5x_get_params(): array
{
    return [
        'url'      => '',
        'scheme'   => '',
        'format'   => 'xml',
        'download' => false,
        'parse'    => false,
        'generate' => false,
        'output'   => false,
        'debug'    => true,
        'verbose'  => false
    ];
}

/**
 * @return array
 * @throws Exception
 */
function p5x_get_cli_params(): array
{
    $params = p5x_get_params();

    $options = getopt('', ['url:', 'scheme:', 'format::', 'download::', 'parse::', 'generate::', 'output::', 'debug::', 'verbose::']);
    p5x_validate_params($options);

    return array_merge($params, $options);
}

/**
 * @return array
 * @throws Exception
 */
function p5x_get_http_params(): array
{
    $params = p5x_get_params();

    if (!empty($_GET['url'])) {
        $params['url'] = $_GET['url'];
    }

    if (!empty($_GET['scheme'])) {
        $params['scheme'] = $_GET['scheme'];
    }

    if (!empty($_GET['format'])) {
        $params['format'] = $_GET['format'];
    }

    if (!empty($_GET['download'])) {
        $params['download'] = true;
    }

    if (!empty($_GET['parse'])) {
        $params['parse'] = true;
    }

    if (!empty($_GET['generate'])) {
        $params['generate'] = true;
    }

    if (!empty($_GET['output'])) {
        $params['output'] = true;
    }

    if (isset($_GET['debug'])) {
        $params['debug'] = (bool) $_GET['debug'];
    }

    if (!empty($_GET['verbose'])) {
        $params['verbose'] = true;
    }

    p5x_validate_params($params);

    return $params;
}

/**
 * @param array $source
 *
 * @throws Exception
 */
function p5x_validate_params(array $source)
{
    if (empty($source['url'])) {
        throw new \EntegreX\DataMapper\RuntimeException('URL not defined!');
    }
}

function p5x_json_error_output(int $errCode, string $errMessage): string
{
    return json_encode(['success' => false, 'error_code' => $errCode, 'error_message' => $errMessage]);
}